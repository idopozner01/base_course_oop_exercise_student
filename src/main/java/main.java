import AerialVehicles.F16;
import AerialVehicles.Zik;
import Entities.Coordinates;
import Missions.AttackMission;
import Missions.BdaMission;
import Missions.IntelligenceMission;

public class main {
  public static void main(String[] args) throws Exception {
    F16 f16 = new F16(250, new Coordinates(22.22, 11.11));
    AttackMission attackMission =
        new AttackMission(
            new Coordinates(33.33, 66.66), "Danny Dito", f16, "scary home", 3, "Python");
    attackMission.begin();
    attackMission.finish();

    Zik zik = new Zik(100, new Coordinates(87.54, -65.21));
    IntelligenceMission intelligenceMission =
        new IntelligenceMission(
            new Coordinates(101.54, 4.8), "John Biryon", zik, "Norway", "InfraRed");
    intelligenceMission.begin();
    intelligenceMission.finish();

    intelligenceMission.begin();
    intelligenceMission.cancel();

    BdaMission bdaMission =
        new BdaMission(
            new Coordinates(907.54, 420.69), "Big Chungus", zik, "ice cream truck", "Thermal");
    bdaMission.begin();
    bdaMission.finish();

    zik.hoverOverLocation(new Coordinates(800.800, 900.123));
  }
}
