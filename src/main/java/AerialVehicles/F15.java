package AerialVehicles;

import Abilities.Ability;
import Abilities.Attacker;
import Abilities.BdaAssessor;
import Entities.Coordinates;

public class F15 extends AerialVehicle {
  public F15(int maxHoursOfFlight, Coordinates homeBase) {
    super(maxHoursOfFlight, homeBase);
  }

  @Override
  protected boolean isValidAbility(Ability ability) {
    if (ability instanceof Attacker || ability instanceof BdaAssessor) {
      return true;
    }
    return false;
  }
}
