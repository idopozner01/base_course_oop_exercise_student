package Exceptions;

public class CameraTypeNotCompatibleException extends Exception {
  public CameraTypeNotCompatibleException(String message) {
    super(message);
  }
}
